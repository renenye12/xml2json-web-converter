import 'package:flutter/material.dart';
import 'package:xml2json_web_converter/converter_view.dart';

void main() {
  runApp(Json2xmlWebConverterApp());
}

class Json2xmlWebConverterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Json2xml Web Converter',
      theme: ThemeData(
        primaryColor: Colors.teal,
      ),
      home: ConverterView(),
    );
  }
}
